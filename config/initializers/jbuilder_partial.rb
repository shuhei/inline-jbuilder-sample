require 'jbuilder'

ActiveSupport.on_load :action_view do
  ActionView::Template.unregister_template_handler :jbuilder
  ActionView::Template.register_template_handler :jbuilder, JbuilderInlinePartials::Handler
end
