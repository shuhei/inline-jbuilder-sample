class TestsController < ApplicationController
  layout false
  before_action :setup_data

  def inline
  end

  def nested
  end

  def to_json
    render json: { breweries: @breweries }.to_json
  end

  def as_json
    render json: { breweries: @breweries }.as_json
  end

  private

  def setup_data
    @breweries = (0...50).map { Brewery.make_random }
  end
end
