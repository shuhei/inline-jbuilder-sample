json.breweries @breweries do |brewery|
  json.extract! brewery, :name
  json.country do
    json.extract! brewery.country, :name
  end
  json.beers brewery.beers do |beer|
    json.extract! beer, :name
    json.style do
      json.extract! beer.style, :name
    end
    json.reviews beer.reviews do |review|
      json.extract! review, :score, :body
      json.reviewer do
        json.extract! review.reviewer, :name
      end
    end
  end
end
