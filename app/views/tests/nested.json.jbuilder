json.breweries @breweries do |brewery|
  json.partial! 'partials/brewery', brewery: brewery
end
