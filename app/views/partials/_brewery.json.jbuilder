json.extract! brewery, :name
json.country do
  json.partial! 'partials/country', country: brewery.country
end
json.beers brewery.beers do |beer|
  json.partial! 'partials/beer', beer: beer
end
