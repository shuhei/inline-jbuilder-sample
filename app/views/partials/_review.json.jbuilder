json.extract! review, :score, :body
json.reviewer do
  json.partial! 'partials/user', user: review.reviewer
end
