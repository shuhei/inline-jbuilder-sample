json.extract! beer, :name
json.style do
  json.partial! 'partials/style', style: beer.style
end
json.reviews beer.reviews do |review|
  json.partial! 'partials/review', review: review
end
