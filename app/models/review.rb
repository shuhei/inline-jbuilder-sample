class Review
  include ActiveModel::Model

  attr_accessor :score, :body, :reviewer

  def self.make_random
    reviewer = User.make_random
    new(score: rand(100), body: SecureRandom.hex, reviewer: reviewer)
  end
end
