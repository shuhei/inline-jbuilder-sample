class User
  include ActiveModel::Model

  attr_accessor :name

  def self.make_random
    new(name: SecureRandom.hex)
  end
end
