class Brewery
  include ActiveModel::Model

  attr_accessor :name, :country, :beers

  def self.make_random
    country = Country.make_random
    beers = (0...10).map { Beer.make_random }
    new(name: SecureRandom.hex, country: country, beers: beers)
  end
end
