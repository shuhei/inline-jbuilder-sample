class Beer
  include ActiveModel::Model

  attr_accessor :name, :style, :reviews

  def self.make_random
    style = Style.make_random
    reviews = (0...3).map { Review.make_random }
    new(name: SecureRandom.hex, style: style, reviews: reviews)
  end
end
